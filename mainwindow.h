#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QNetworkAccessManager>
#include <QLabel>
#include "mpvplayer.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void onStartBtnClicked();
    void onSubmitBtnClicked();
    void onMuteBtnClicked();
    void onVolumeSliderChanged();
    void onSpeedBoxChanged(double speed);
    void onDurationChanged(int64_t ttime);
    void onTimePosChanged(int64_t ttime);
    void onProgressSliderChanged();
    void onPlayEnd();
    void onFileOpen();
    void initLoadingWidget();

    void hlsRequestFailedSlots(const QString);
    void pieceDownloadFailSlots(int idx, const QString &msg);
    void hlsPlayErrorSlots();
    void pieceLoadingSlots();
    void pieceLoadedSlots();
private:
    void start();
    void stop();
private:
    Ui::MainWindow *ui;
    QWidget *coverWidget;
    QLabel *_loading_label;
    QNetworkAccessManager manager;
    bool _stop_flag;
    bool _mute_flag;
    int64_t _cur_duration;
    QString _play_file;
    MpvPlayer *_mpv_player;
};
#endif // MAINWINDOW_H
