#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMenu>
#include <QAction>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QVBoxLayout>
#include "m3u8parser.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , _stop_flag(true)
    , _mute_flag(false)
{
    ui->setupUi(this);
    this->setWindowTitle("测试播放器");
    this->setWindowIcon(QIcon(":/res/res/media.png"));
    QMenu *menu = ui->menubar->addMenu("文件");
    QAction *on_open = new QAction("打开", this);
    on_open->setShortcuts(QKeySequence::Open);
    on_open->setStatusTip("打开视频文件");
    connect(on_open, &QAction::triggered, this, &MainWindow::onFileOpen);
    menu->setIcon(QIcon(":/res/res/wenjian.png"));
    menu->addAction(on_open);

    _mpv_player = new MpvPlayer(ui->playerWidget);
    ui->startBtn->setDisabled(true);
    ui->startBtn->setIcon(QIcon(":/res/res/bofang1.png"));

    ui->muteBtn->setIcon(QIcon(":/res/res/diyinliang.png"));

    ui->urlEdit->setText("http://47.109.70.70:9090/video/ceshi.m3u8");

    ui->playerWidget->setStyleSheet("background-color:black;");
    initLoadingWidget();

    connect(_mpv_player, &MpvPlayer::durationChangedSignal, this, &MainWindow::onDurationChanged);
    connect(_mpv_player, &MpvPlayer::timePosChangedSignal, this, &MainWindow::onTimePosChanged);
    connect(_mpv_player, &MpvPlayer::mpvPalyEnd, this, &MainWindow::onPlayEnd);
    connect(ui->startBtn, &QPushButton::clicked, this, &MainWindow::onStartBtnClicked);
    connect(ui->volumeSlider, &QSlider::sliderReleased, this, &MainWindow::onVolumeSliderChanged);
    connect(ui->muteBtn, &QPushButton::clicked, this, &MainWindow::onMuteBtnClicked);
    connect(ui->speedBox, &QDoubleSpinBox::valueChanged, this, &MainWindow::onSpeedBoxChanged);
    connect(ui->progressSlider, &QSlider::sliderReleased, this, &MainWindow::onProgressSliderChanged);
    connect(ui->submitBtn, &QPushButton::clicked, this, &MainWindow::onSubmitBtnClicked);

    connect(_mpv_player, &MpvPlayer::hlsRequestFailedSignal, this, &MainWindow::hlsRequestFailedSlots);
    connect(_mpv_player, &MpvPlayer::pieceDownloadFailSignal, this, &MainWindow::pieceDownloadFailSlots);
    connect(_mpv_player, &MpvPlayer::hlsPlayErrorSignal, this, &MainWindow::hlsPlayErrorSlots);
    connect(_mpv_player, &MpvPlayer::pieceLoadingSignal, this, &MainWindow::pieceLoadingSlots);
    connect(_mpv_player, &MpvPlayer::pieceLoadedSignal, this, &MainWindow::pieceLoadedSlots);
}

void MainWindow::hlsRequestFailedSlots(const QString msg){
    ui->statusbar->showMessage(msg);
}
void MainWindow::pieceDownloadFailSlots(int idx, const QString &msg){
    ui->statusbar->showMessage(msg);
}
void MainWindow::hlsPlayErrorSlots(){
    ui->statusbar->showMessage("视频播放出错");
}
void MainWindow::pieceLoadingSlots(){
    ui->coverWidget->show();
}
void MainWindow::pieceLoadedSlots(){
    ui->coverWidget->hide();
}

void MainWindow::initLoadingWidget() {
    QVBoxLayout *layout = new QVBoxLayout;
    _loading_label = new QLabel;
    QFont ft("黑体", 12, 87);
    _loading_label->setFont(ft);
    _loading_label->setText("加载中...");
    _loading_label->setStyleSheet("color:white;");
    _loading_label->setAlignment(Qt::AlignCenter);
    _loading_label->setAttribute(Qt::WA_TranslucentBackground);
    layout->addWidget(_loading_label);
    ui->coverWidget->setLayout(layout);
    ui->coverWidget->setWindowOpacity(0.5);
    QPalette palette = ui->coverWidget->palette();
    palette.setColor(QPalette::Window, Qt::gray);
    ui->coverWidget->setPalette(palette);
    ui->coverWidget->raise();
    ui->coverWidget->hide();
}

void MainWindow::start() {
    _stop_flag = false;
    ui->startBtn->setIcon(QIcon(":/res/res/zanting.png"));
    ui->startBtn->setDisabled(false);
    if (_mpv_player) _mpv_player->start();
}
void MainWindow::stop() {
    _stop_flag = true;
    ui->startBtn->setIcon(QIcon(":/res/res/bofang1.png"));
    ui->startBtn->setDisabled(false);
    if (_mpv_player) _mpv_player->stop();
}
void MainWindow::onFileOpen() {
    _play_file = QFileDialog::getOpenFileName(this, "打开文件");
    qDebug() << "打开文件：" << _play_file;
    if (_mpv_player) {
        _cur_duration = 0;
        _mpv_player->playLocalVideo(_play_file);
        _mpv_player->setVolume(60);
        ui->volumeSlider->setSliderPosition(60);
        this->stop();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::onStartBtnClicked() {
    if (_stop_flag == true) {
        this->start();
    }else {
        this->stop();
    }

}
void MainWindow::onSubmitBtnClicked() {
    QString qurl = ui->urlEdit->text();
    qDebug() << "获取到URL：" << qurl;
    if (_mpv_player) {
        _mpv_player->playHLSVideo(qurl);
        _mpv_player->setVolume(60);
        ui->volumeSlider->setSliderPosition(60);
        this->stop();
    }
}
void MainWindow::onProgressSliderChanged() {
    int64_t pos = ui->progressSlider->value();
    if (_mpv_player) _mpv_player->setPos(pos);
    qDebug() << "跳转播放位置：" << pos;
}
void MainWindow::onSpeedBoxChanged(double speed) {
    if (_mpv_player) _mpv_player->setSpeed(speed);
    qDebug() << "设置播放速度：" << speed;
}
void MainWindow::onMuteBtnClicked() {
    _mute_flag = !_mute_flag;
    if (_mpv_player) _mpv_player->setMute(_mute_flag);
    if (_mute_flag){
        ui->muteBtn->setIcon(QIcon(":/res/res/jingyin.png"));
    }else{
        ui->muteBtn->setIcon(QIcon(":/res/res/diyinliang.png"));
    }
}
void MainWindow::onVolumeSliderChanged() {
    int64_t pos = ui->volumeSlider->value();
    if (_mpv_player)_mpv_player->setVolume(pos);
    qDebug() << "设置音量：" << pos;
}
void MainWindow::onTimePosChanged(int64_t ttime) {
    qDebug() << "收到播放时间改变信号,调整播放进度条位置:" << ttime ;
    QString time_pos_str = QTime(0,0,0).addSecs(ttime).toString("HH:mm:ss");
    QString time_total_str = QTime(0,0,0).addSecs(_cur_duration).toString("HH:mm:ss");
    ui->progressLabel->setText(time_pos_str + " / " + time_total_str);
    ui->progressSlider->setValue(ttime);
}
void MainWindow::onDurationChanged(int64_t ttime) {
    _cur_duration = ttime;
    QString time_str = QTime(0,0,0).addSecs(ttime).toString("HH:mm:ss");
    ui->progressLabel->setText("00:00:00 / " + time_str);
    ui->progressSlider->setRange(0, _cur_duration);
}
void MainWindow::onPlayEnd() {
    qDebug() << "主窗口收到信号：视频播放结束！";
}

