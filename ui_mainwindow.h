/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *playerWidget;
    QPushButton *startBtn;
    QSlider *progressSlider;
    QDoubleSpinBox *speedBox;
    QLineEdit *urlEdit;
    QPushButton *submitBtn;
    QPushButton *muteBtn;
    QSlider *volumeSlider;
    QLabel *progressLabel;
    QWidget *coverWidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        playerWidget = new QWidget(centralwidget);
        playerWidget->setObjectName("playerWidget");
        playerWidget->setGeometry(QRect(60, 50, 681, 331));
        startBtn = new QPushButton(centralwidget);
        startBtn->setObjectName("startBtn");
        startBtn->setGeometry(QRect(60, 420, 75, 41));
        progressSlider = new QSlider(centralwidget);
        progressSlider->setObjectName("progressSlider");
        progressSlider->setGeometry(QRect(60, 390, 681, 22));
        progressSlider->setOrientation(Qt::Orientation::Horizontal);
        speedBox = new QDoubleSpinBox(centralwidget);
        speedBox->setObjectName("speedBox");
        speedBox->setGeometry(QRect(670, 420, 62, 41));
        speedBox->setMinimum(0.500000000000000);
        speedBox->setMaximum(3.000000000000000);
        speedBox->setSingleStep(0.250000000000000);
        speedBox->setValue(1.000000000000000);
        urlEdit = new QLineEdit(centralwidget);
        urlEdit->setObjectName("urlEdit");
        urlEdit->setGeometry(QRect(60, 480, 621, 41));
        submitBtn = new QPushButton(centralwidget);
        submitBtn->setObjectName("submitBtn");
        submitBtn->setGeometry(QRect(690, 480, 51, 41));
        muteBtn = new QPushButton(centralwidget);
        muteBtn->setObjectName("muteBtn");
        muteBtn->setGeometry(QRect(370, 420, 75, 41));
        volumeSlider = new QSlider(centralwidget);
        volumeSlider->setObjectName("volumeSlider");
        volumeSlider->setGeometry(QRect(450, 430, 160, 22));
        volumeSlider->setOrientation(Qt::Orientation::Horizontal);
        progressLabel = new QLabel(centralwidget);
        progressLabel->setObjectName("progressLabel");
        progressLabel->setGeometry(QRect(150, 420, 121, 41));
        coverWidget = new QWidget(centralwidget);
        coverWidget->setObjectName("coverWidget");
        coverWidget->setGeometry(QRect(60, 49, 681, 331));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 800, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        startBtn->setText(QString());
        submitBtn->setText(QCoreApplication::translate("MainWindow", "\347\241\256\345\256\232", nullptr));
        muteBtn->setText(QString());
        progressLabel->setText(QCoreApplication::translate("MainWindow", "00:00:00 / 00:00:00", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
